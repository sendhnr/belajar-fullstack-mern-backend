import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import morgan from "morgan";
// const connect = require('connect')

import UserRoute from "./routes/UserRoute.js"

const app = express();
mongoose.connect('mongodb://localhost:27017/fullstackdb',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', (error) => console.log(error));
db.once('open', () => console.log('Database Connected...'))


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan('dev'));

app.use(UserRoute);

app.listen(5000, () => {
    console.log(`Server started on port`);
});