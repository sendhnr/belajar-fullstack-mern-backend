import express from "express";

const router = express.Router();

import { getUsers, getUsersById, saveUser, updateUser, deleteUser } from "../controllers/UserController.js"

router.get('/api/users', getUsers);
router.get('/api/users/:id', getUsersById);
router.post('/api/users', saveUser);
router.patch('/api/users/:id', updateUser);
router.delete('/api/users/:id', deleteUser);

//view routes
router.get('/users', getUsers);
router.get('/users/:id', getUsersById);
router.post('/users', saveUser)
router.patch('/users/:id', updateUser)
router.delete('/users/:id', deleteUser)

export default router;